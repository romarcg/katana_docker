#!/usr/bin/env python
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from sensor_msgs.msg import JointState
from ar_track_alvar_msgs.msg import AlvarMarkers
from ar_track_alvar_msgs.msg import AlvarMarker
## END_SUB_TUTORIAL

from std_msgs.msg import String

class KatanaState():

  def __init__(self):
    ## Setup
    ## ^^^^^
    ##
    ## First initialize moveit_commander and rospy.
    print "============ Starting moveit_commander"
    moveit_commander.roscpp_initialize(sys.argv)
    self.robot = moveit_commander.RobotCommander()
    self.scene = moveit_commander.PlanningSceneInterface()
    self.group = moveit_commander.MoveGroupCommander("arm")
    self.gripper = moveit_commander.MoveGroupCommander("gripper")

    self.group.set_num_planning_attempts(10)
    self.group.set_goal_tolerance(0.03)
    self.group.set_planning_time(5)


    #self.group.set_end_effector_link("katana_gripper_tool_frame")


    rospy.Subscriber("/joint_states", JointState, self.callback_reader)

    self.count = 0

    #self.current_pose_marker = AlvarMarker()

    #self.one_time = False

  def callback_reader(self, data):
    #rospy.loginfo(rospy.get_caller_id() + "data %s", data.data)
    self.get_current_pose()

      #rospy.sleep(5)

      #rospy.shutdown()


  def get_current_pose(self):
    self.count = (self.count + 1) % 10
    if self.count == 0:
      print "============ Printing robot state"
      print self.robot.get_current_state()
      print "============ Printing gripper group"
      print self.gripper.get_current_joint_values()      
      print "============ Printing ee pose"
      print self.group.get_current_pose().pose
      print "============"



  def publish_state(self):

    ## Instantiate a RobotCommander object.  This object is an interface to
    ## the robot as a whole.
    #robot = moveit_commander.RobotCommander()

    ## Instantiate a PlanningSceneInterface object.  This object is an interface
    ## to the world surrounding the robot.
    #scene = moveit_commander.PlanningSceneInterface()

    ## Instantiate a MoveGroupCommander object.  This object is an interface to one group of joints.
    ## This interface can be used to plan and execute motions on the left
    ## arm.
    #group = moveit_commander.MoveGroupCommander("arm")


    ## We create this DisplayTrajectory publisher which is used below to publish
    ## trajectories for RVIZ to visualize.
    #display_trajectory_publisher = rospy.Publisher(
    #                                    '/move_group/display_planned_path',
    #                                    moveit_msgs.msg.DisplayTrajectory,queue_size=5)

    ## Wait for RVIZ to initialize. This sleep is ONLY to allow Rviz to come up.
    ##print "============ Waiting for RVIZ..."
    #r#ospy.sleep(10)

    #group.set_planner_id("IKFastKinematicsPlugin")
    print "============ Starting planning/execution process "

    ## Getting Basic Information
    ## ^^^^^^^^^^^^^^^^^^^^^^^^^
    ##
    ## We can get the name of the reference frame for this robot
    print "============ Reference frame: %s" % self.group.get_planning_frame()
    #group.set_pose_reference_frame("/katana_base_link")

    ## We can also print the name of the end-effector link for this group

    print "============ Reference frame: %s" %  self.group.get_end_effector_link()

    #self.group.set_end_effector_link("katana_gripper_link")

    print "============ New reference frame: %s" %  self.group.get_end_effector_link()

    ## We can get a list of all the groups in the robot
    print "============ Robot Groups:"
    print self.robot.get_group_names()      



    ## Sometimes for debugging it is useful to print the entire state of the
    ## robot.
    if False:
      print "============ Printing robot state"
      print self.robot.get_current_state()
      print "============ Printing ee pose"
      print self.group.get_current_pose().pose
      print "============"


      ## Planning to a Pose goal
      print "============ Planning to a Pose goal"
      ## ^^^^^^^^^^^^^^^^^^^^^^^
      ## We can plan a motion for this group to a desired pose for the
      ## end-effector
      print "============ Generating plan 1"
        #x: 0.0247508438196
        #y: 0.238068788808
        #z: 0.612619750418
        #orientation:
        #x: 0.297805650149
        #y: 0.942263795188
        #z: -0.150006471888
        #w: 0.0308024902946
      #pose_rel = group.get_current_pose().pose

      print "===== target pose selected "
      print "x: ", self.current_pose_marker.pose.pose.position.x
      print "y: ", self.current_pose_marker.pose.pose.position.y
      print "z: ", self.current_pose_marker.pose.pose.position.z

      pose_target = geometry_msgs.msg.Pose()
      pose_target.position = self.current_pose_marker.pose.pose.position
      pose_target.position.z = pose_target.position.z + 0.1
      #pose_target.orientation = self.current_pose_marker.pose.pose.orientation
      #pose_target.orientation.x = 0.0278345433843
      #pose_target.orientation.y = -0.00323575982431
      #pose_target.orientation.z = -0.999026347409
      #pose_target.orientation.w = 0.0340752877503

      print "===== target pose selected "
      print "x: ", pose_target.position.x
      print "y: ", pose_target.position.y
      print "z: ", pose_target.position.z
      print "ox: ", pose_target.orientation.x
      print "oy: ", pose_target.orientation.y
      print "oz: ", pose_target.orientation.z
      print "ow: ", pose_target.orientation.w

      #pose_target.position.x = 0.0247508438196
      #pose_target.position.y = 0.238068788808
      #pose_target.position.z = 0.612619750418
      #pose_target.orientation.x = 0.297805650149
      #pose_target.orientation.y = 0.942263795188
      #pose_target.orientation.z = -0.150006471888
      #pose_target.orientation.w = 0.0308024902946


      #pose_rel.position.y = pose_rel.position.y+0.1
      #group.set_pose_target(pose_rel)

      #pose_random = group.get_random_pose()
      #print pose_random

      #group.set_pose_target(pose_random)

      self.group.set_pose_target(pose_target)

      #print group.get_known_constraints()
      self.group.set_num_planning_attempts(10)
      self.group.set_goal_tolerance(0.001)
      #group.set_goal_orientation_tolerance(0.5)

      ## Now, we call the planner to compute the plan
      ## and visualize it if successful
      ## Note that we are just planning, not asking move_group
      ## to actually move the robot
      plan1 = self.group.plan()
      if plan1:
          self.group.go(wait=True)

      ##print "============ Waiting while RVIZ displays plan1..."
      #rospy.sleep(5)


      ## You can ask RVIZ to visualize a plan (aka trajectory) for you.  But the
      ## group.plan() method does this automatically so this is not that useful
      ## here (it just displays the same trajectory again).
      #print "============ Visualizing plan1"
      #display_trajectory = moveit_msgs.msg.DisplayTrajectory()

      #display_trajectory.trajectory_start = robot.get_current_state()
      #display_trajectory.trajectory.append(plan1)
      #display_trajectory_publisher.publish(display_trajectory);

      #print "============ Waiting while plan1 is visualized (again)..."
      #rospy.sleep(5)

    ## When finished shut down moveit_commander.
    #moveit_commander.roscpp_shutdown()


    print "============ STOPPING"


if __name__=='__main__':

  node = KatanaState()
  rospy.init_node('katana_current_pose', anonymous=True)  
  try:
    #move_group_python_interface_katana()
    #node.move_group_python_interface_katana()
    node.publish_state()
    rospy.spin()
  except rospy.ROSInterruptException:
    pass



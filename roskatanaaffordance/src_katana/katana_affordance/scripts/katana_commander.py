#!/usr/bin/env python
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

from moveit_python.planning_scene_interface import PlanningSceneInterface


from std_msgs.msg import String

def move_group_python_interface_katana():
  ## Setup
  ## ^^^^^
  ##
  ## First initialize moveit_commander and rospy.
  print "============ Starting moveit_commander"
  moveit_commander.roscpp_initialize(sys.argv)
  rospy.init_node('katana_commander_single_goal', anonymous=True)

  ## Instantiate a RobotCommander object.  This object is an interface to
  ## the robot as a whole.
  robot = moveit_commander.RobotCommander()

  ## Instantiate a PlanningSceneInterface object.  This object is an interface
  ## to the world surrounding the robot.
  #scene = moveit_commander.PlanningSceneInterface()

  scene = PlanningSceneInterface(robot.get_planning_frame())


  ## Instantiate a MoveGroupCommander object.  This object is an interface to one group of joints.
  ## This interface can be used to plan and execute motions on the left
  ## arm.
  group = moveit_commander.MoveGroupCommander("arm")

  group.set_num_planning_attempts(10)
  group.set_goal_tolerance(0.03)
  group.set_planning_time(5)
  #group.set_end_effector_link("katana_gripper_tool_frame")
  #group.clear_path_constraints()
  #group.clear_pose_target(group.get_end_effector_link())
  #group.clear_pose_targets()
  #group.allow_looking(False)
  #group.allow_replanning(False)

  print "============ Setting-up the planning scene"
  #scene.remove_world_object()
  #plane_pose = geometry_msgs.msg.PoseStamped()
  #plane_pose.pose.position.x = 0
  #plane_pose.pose.position.y = 0
  #plane_pose.pose.position.z = 0

  #position: [-0.075, -1.275, -0.22]   
  #orientation: [0.0, 0.0, 0.0, 1.0]   
  #size: [1.6, 0.8, 0.01]    # Objects Size (Length, Width, Hight)
  #colour: [0.4, 0.4, 0.4, 1.0] 
  scene.clear()
  scene.addBox("support",1.0,1.0,0.5,0,0,-0.25)
  scene.setColor("support", 0.4, 0.4, 0.4, 1.0)
  #rospy.sleep(2)

  ## We create this DisplayTrajectory publisher which is used below to publish
  ## trajectories for RVIZ to visualize.
  #display_trajectory_publisher = rospy.Publisher(
  #                                    '/move_group/display_planned_path',
  #                                    moveit_msgs.msg.DisplayTrajectory,queue_size=5)

  ## Wait for RVIZ to initialize. This sleep is ONLY to allow Rviz to come up.
  #print "============ Waiting for RVIZ..."
  #rospy.sleep(10)

  #group.set_planner_id("")
  print "============ Starting planning/execution process " 

  ## Getting Basic Information
  ## ^^^^^^^^^^^^^^^^^^^^^^^^^
  ##
  ## We can get the name of the reference frame for this robot
  print "============ Planning reference frame: %s" % group.get_planning_frame()
  #group.set_pose_reference_frame("/katana_base_link")

  ## We can also print the name of the end-effector link for this group

  print "============ EE link: %s" %  group.get_end_effector_link()

  #group.set_end_effector_link("katana_gripper_link")

  #print "============ New reference frame: %s" %  group.get_end_effector_link()

  print "============ Pose reference frame: %s" %  group.get_pose_reference_frame()


  ## We can get a list of all the groups in the robot
  print "============ Robot Groups:"
  print robot.get_group_names()

  ## Sometimes for debugging it is useful to print the entire state of the
  ## robot.
  print "============ Printing robot state"
  print robot.get_current_state()
  print "============ Printing ee pose"
  print group.get_current_pose().pose
  print "============ Printing joint values"
  group_variable_values = group.get_current_joint_values()
  print group_variable_values



  ## Planning to a Pose goal
  print "============ Planning to a Pose goal"
  ## optional
  #group.set_start_state_to_current_state()

  print "============ Generating plan 1"

  #pose_rel = group.get_current_pose().pose
  pose_target = geometry_msgs.msg.Pose()
  pose_target.position.x = -0.0721530552669
  pose_target.position.y = -0.112920939536
  pose_target.position.z = 0.430746519753
  pose_target.orientation.x = 0.471373744949 
  pose_target.orientation.y = -0.670010232114
  pose_target.orientation.z = -0.553332847335
  pose_target.orientation.w = 0.150717754414

  #pose_rel.position.y = pose_rel.position.y+0.1
  #group.set_pose_target(pose_rel)

  #pose_random = group.get_random_pose()
  #print pose_random

  #group.set_pose_target(pose_random)

  print pose_target
  #group.set_position_target([pose_target.position.x, pose_target.position.y, pose_target.position.z])

  #group.set_pose_target(pose_target)
  group.set_pose_target([0.134261754689, -0.0413985143412, 0.46121980208, 0.789435495945, -0.0715663992969, -0.150807453701, 0.590700397954])


  #group.shift_pose_target(0,0.5)

  #print group.get_known_constraints()
  

  ## Now, we call the planner to compute the plan
  ## and visualize it if successful
  ## Note that we are just planning, not asking move_group
  ## to actually move the robot
  plan1 = group.plan()
  #print plan1
  if len(plan1.joint_trajectory.points) > 0:
      print "******* A plan was found ********"
      #print plan1
      group.go(wait=True)
  else:
      print "No plan was found"


  ##
  #planning with joints is working quite qwell
  ##
  #group_variable_values = group.get_current_joint_values()
  #print "============ Joint values: ", group_variable_values

  #group_variable_values[2] = 1.0
  #group.set_joint_value_target(group_variable_values)

  #plan2 = group.plan()


  ##print "============ Waiting while RVIZ displays plan1..."
  ##rospy.sleep(5)


  ## You can ask RVIZ to visualize a plan (aka trajectory) for you.  But the
  ## group.plan() method does this automatically so this is not that useful
  ## here (it just displays the same trajectory again).
  #print "============ Visualizing plan1"
  #display_trajectory = moveit_msgs.msg.DisplayTrajectory()

  #display_trajectory.trajectory_start = robot.get_current_state()
  #display_trajectory.trajectory.append(plan1)
  #display_trajectory_publisher.publish(display_trajectory);

  #print "============ Waiting while plan1 is visualized (again)..."
  #rospy.sleep(5)



  if False:
      ## Moving to a pose goal
      ## ^^^^^^^^^^^^^^^^^^^^^
      ##
      ## Moving to a pose goal is similar to the step above
      ## except we now use the go() function. Note that
      ## the pose goal we had set earlier is still active
      ## and so the robot will try to move to that goal. We will
      ## not use that function in this tutorial since it is
      ## a blocking function and requires a controller to be active
      ## and report success on execution of a trajectory.

      # Uncomment below line when working with a real robot
      # group.go(wait=True)

      ## Planning to a joint-space goal
      ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      print "============ Planning to a joint-space goal"
      ##
      ## Let's set a joint space goal and move towards it.
      ## First, we will clear the pose target we had just set.

      group.clear_pose_targets()

      ## Then, we will get the current set of joint values for the group
      group_variable_values = group.get_current_joint_values()
      print "============ Joint values: ", group_variable_values

      ## Now, let's modify one of the joints, plan to the new joint
      ## space goal and visualize the plan
      group_variable_values[0] = -2.0
      group.set_joint_value_target(group_variable_values)

      plan2 = group.plan()

      # uncomment when working with real robot
      group.go(wait=True)

      #print "============ Waiting while RVIZ displays plan2..."
      #rospy.sleep(5)


      ## Cartesian Paths
      ## ^^^^^^^^^^^^^^^
      print "============ Planning a cartesian path"
      ## You can plan a cartesian path directly by specifying a list of waypoints
      ## for the end-effector to go through.
      waypoints = []

      # start with the current pose
      waypoints.append(group.get_current_pose().pose)

      # first orient gripper and move forward (+x)
      wpose = geometry_msgs.msg.Pose()
      wpose.orientation.w = 1.0
      wpose.position.x = waypoints[0].position.x + 0.1
      wpose.position.y = waypoints[0].position.y
      wpose.position.z = waypoints[0].position.z
      waypoints.append(copy.deepcopy(wpose))

      # second move down
      wpose.position.z -= 0.10
      waypoints.append(copy.deepcopy(wpose))

      # third move to the side
      wpose.position.y += 0.05
      waypoints.append(copy.deepcopy(wpose))

      ## We want the cartesian path to be interpolated at a resolution of 1 cm
      ## which is why we will specify 0.01 as the eef_step in cartesian
      ## translation.  We will specify the jump threshold as 0.0, effectively
      ## disabling it.
      (plan3, fraction) = group.compute_cartesian_path(
                                   waypoints,   # waypoints to follow
                                   0.01,        # eef_step
                                   0.0)         # jump_threshold

      #print "============ Waiting while RVIZ displays plan3..."
      #rospy.sleep(5)


      ## Adding/Removing Objects and Attaching/Detaching Objects
      ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      print "============ Planning adding/removing and attaching/detaching objects"
      ## First, we will define the collision object message
      #collision_object = moveit_msgs.msg.CollisionObject()



  ## When finished shut down moveit_commander.
  moveit_commander.roscpp_shutdown()

  print "============ STOPPING"


if __name__=='__main__':
  try:
    move_group_python_interface_katana()
  except rospy.ROSInterruptException:
    pass



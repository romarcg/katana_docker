#!/usr/bin/env python
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from ar_track_alvar_msgs.msg import AlvarMarkers
from ar_track_alvar_msgs.msg import AlvarMarker

from moveit_python.planning_scene_interface import PlanningSceneInterface

from std_msgs.msg import String

from tf import TransformListener

import tf

from find_object_2d.msg import ObjectsStamped

class MoverMarker():

  def __init__(self, object_tf):
    ## Setup
    ## ^^^^^
    ##
    ## First initialize moveit_commander and rospy.
    self.tf_listener = TransformListener()
    self.current_pose_marker = AlvarMarker()
    self.now_executing = False
    self.object_tf = object_tf

    print "============ Starting moveit_commander"
    moveit_commander.roscpp_initialize(sys.argv)

    ## Instantiate a RobotCommander object.  This object is an interface to
    ## the robot as a whole.
    self.robot = moveit_commander.RobotCommander()

    ## Instantiate a PlanningSceneInterface object.  This object is an interface
    ## to the world surrounding the robot.
    #scene = moveit_commander.PlanningSceneInterface()
    self.scene = PlanningSceneInterface(self.robot.get_planning_frame())

    print "============ Setting-up the planning scene"
    #position: [-0.075, -1.275, -0.22]
    #orientation: [0.0, 0.0, 0.0, 1.0]
    #size: [1.6, 0.8, 0.01]    # Objects Size (Length, Width, Hight)
    #colour: [0.4, 0.4, 0.4, 1.0]
    self.scene.clear()
    self.scene.addBox("support",1.0,1.0,0.5,0,0,-0.26)
    self.scene.setColor("support", 0.3, 0.3, 0.3, 1.0)

    ## Instantiate a MoveGroupCommander object.  This object is an interface to one group of joints.
    ## This interface can be used to plan and execute motions on the left
    ## arm.
    self.group = moveit_commander.MoveGroupCommander("arm")
    self.group.set_num_planning_attempts(10)
    self.group.set_goal_tolerance(0.03)
    self.group.set_planning_time(5)
    self.group.set_max_velocity_scaling_factor(0.8)
    self.group.allow_replanning(True)
    #group.set_end_effector_link("katana_gripper_link")
    #group.set_goal_orientation_tolerance(0.1)

    self.gripper = moveit_commander.MoveGroupCommander("gripper")

    ##
    # fixed poses (positions and orientations)
    ##
    self.initial_arm_position_joints = [0.06317273769132603, 2.163014533126086, -1.2253699112816752, 1.6439501661460652, -2.990048987131473]
    self.gripper_open_joints = [0.4, 0.4]
    self.gripper_full_closed_joints = [-0.42, -0.42]

    self.last_position = [0, 0, 0]
    self.last_orientation = [0, 0, 0, 1]

    ## We create this DisplayTrajectory publisher which is used below to publish
    ## trajectories for RVIZ to visualize.
    self.display_trajectory_publisher = rospy.Publisher(
                                        '/move_group/display_planned_path',
                                        moveit_msgs.msg.DisplayTrajectory,queue_size=5)

    rospy.Subscriber("/ar_pose_marker", AlvarMarkers, self.callback_reader_alvar)
    rospy.Subscriber("/objectsStamped", ObjectsStamped, self.callback_reader_find_object)

    ## Wait for RVIZ to initialize. This sleep is ONLY to allow Rviz to come up.
    ##print "============ Waiting for RVIZ..."
    #r#ospy.sleep(10)

    self.go_to_initial_position()

  def callback_reader_alvar(self, data):
    #rospy.loginfo(rospy.get_caller_id() + "data %s", data.data)
    if False:
        array_markers = data.markers
        if len(array_markers) > 0 and self.now_executing == False:
          self.now_executing = True
          self.current_pose_marker = array_markers[0]
          print "===== first marker pose"
          print "x: ", self.current_pose_marker.pose.pose.position.x
          print "y: ", self.current_pose_marker.pose.pose.position.y
          print "z: ", self.current_pose_marker.pose.pose.position.z

          self.move_group_python_interface_katana()

      #rospy.sleep(5)

      #rospy.shutdown()

  def callback_reader_find_object(self, data):
    object_tf = self.object_tf
    if self.now_executing == False:
        try:
            if self.tf_listener.frameExists("/katana_base_link") and self.tf_listener.frameExists(object_tf):
                t = self.tf_listener.getLatestCommonTime("/katana_base_link", object_tf)
                position, quaternion = self.tf_listener.lookupTransform("/katana_base_link", object_tf, t)
                #print position, quaternion
                if self.last_position != position:
                    self.last_position = position
                    self.last_orientation = quaternion
                    self.now_executing = True
                    self.current_pose_marker.pose.pose.position.x = position[0]
                    self.current_pose_marker.pose.pose.position.y = position[1]
                    self.current_pose_marker.pose.pose.position.z = position[2]
                    self.current_pose_marker.pose.pose.orientation.x = quaternion[0]
                    self.current_pose_marker.pose.pose.orientation.y = quaternion[1]
                    self.current_pose_marker.pose.pose.orientation.z = quaternion[2]
                    self.current_pose_marker.pose.pose.orientation.w = quaternion[3]

                    print "===== object detected at: "
                    print self.current_pose_marker.pose.pose.position

                    self.move_group_python_interface_katana()
                else:
                    print "-same position as before, nothing to be done-"
            else:
                print ">> tf to object is not available: ", object_tf
        except tf.ExtrapolationException, tf.TransformException:
            pass


  def go_to_initial_position(self):
    print "============ Arm to initial position"
    self.group.clear_pose_targets()
    #self.initial_arm_position_joints = group.get_current_joint_values()

    self.group.set_joint_value_target(self.initial_arm_position_joints)

    plan_initial = self.group.plan()

    if len(plan_initial.joint_trajectory.points) > 0:
      print "******* Plan found to move to initial position ********"
      self.group.go(wait=True)

      ## putting gripper to initial position
      #self.gripper.set_joint_value_target(self.gripper_full_closed_joints)
      #plan_full_close_gripper = self.gripper.plan()
      #if len(plan_full_close_gripper.joint_trajectory.points) > 0:
      #  print "******* Closing gripper "
      #  self.gripper.go(wait=True)
      #else:
      #  print "******* Unable to close gripper"

    else:
      print "------- No plan was found to initial position --------"

  def move_group_python_interface_katana(self):

    ##print "============ Waiting while RVIZ displays plan1..."
    #rospy.sleep(2)

    #group.set_planner_id("IKFastKinematicsPlugin")
    print "============ Starting planning/execution process "

    ## Getting Basic Information
    ## ^^^^^^^^^^^^^^^^^^^^^^^^^
    ##
    ## We can get the name of the reference frame for this robot
    print "============ Reference frame: %s" % self.group.get_planning_frame()
    #group.set_pose_reference_frame("/katana_base_link")

    ## We can also print the name of the end-effector link for this group

    print "============ Reference frame: %s" %  self.group.get_end_effector_link()

    print "============ New reference frame: %s" %  self.group.get_end_effector_link()

    ## We can get a list of all the groups in the robot
    print "============ Robot Groups:"
    print self.robot.get_group_names()

    ## Sometimes for debugging it is useful to print the entire state of the
    ## robot.
    if False:
      print "============ Printing robot state"
      print self.robot.get_current_state()
      print "============ Printing ee pose"
      print self.group.get_current_pose().pose
      print "============"


    ## Planning to a Pose goal
    print "============ Planning to a Pose goal"
    ## ^^^^^^^^^^^^^^^^^^^^^^^
    ## We can plan a motion for this group to a desired pose for the
    ## end-effector
    print "============ Generating plan 1"
      #x: 0.0247508438196
      #y: 0.238068788808
      #z: 0.612619750418
      #orientation:
      #x: 0.297805650149
      #y: 0.942263795188
      #z: -0.150006471888
      #w: 0.0308024902946
    #pose_rel = group.get_current_pose().pose

    print "===== target pose selected "
    print ">> target pose: ", self.current_pose_marker.pose.pose
    #print "x: ", self.current_pose_marker.pose.pose.position.x
    #print "y: ", self.current_pose_marker.pose.pose.position.y
    #print "z: ", self.current_pose_marker.pose.pose.position.z

    #orientation 1
    #x: -0.00537014346796
    #y: 0.570391439029
    #z: -0.00147036256805
    #w: 0.821354129396

    #orientation 2
    #x: 0.815496991992
    #y: 0.0387676099597
    #z: -0.574498681216
    #w: 0.0584208331864

    fixed_o1 = geometry_msgs.msg.Quaternion()
    fixed_o1.x = -0.00537014346796
    fixed_o1.y = 0.570391439029
    fixed_o1.z = -0.00147036256805
    fixed_o1.w = 0.821354129396

    fixed_o2 = geometry_msgs.msg.Quaternion()
    fixed_o2.x = 0.815496991992
    fixed_o2.y = 0.0387676099597
    fixed_o2.z = -0.574498681216
    fixed_o2.w = 0.0584208331864

    pose_target = geometry_msgs.msg.Pose()
    pose_target.position = self.current_pose_marker.pose.pose.position
    #pose_target.orientation = self.current_pose_marker.pose.pose.orientation
    pose_target.orientation = fixed_o1
    #pose_target.position.z = pose_target.position.z + 0.1

    print "===== target pose selected "
    print "x: ", pose_target.position.x
    print "y: ", pose_target.position.y
    print "z: ", pose_target.position.z

    print "ox: ", pose_target.orientation.x
    print "oy: ", pose_target.orientation.y
    print "oz: ", pose_target.orientation.z
    print "ow: ", pose_target.orientation.w

    offset_x = 0.02
    offset_y = 0.05
    offset_z = 0.01
    pose_target.position.x = pose_target.position.x - offset_x
    pose_target.position.y = pose_target.position.y - offset_y
    pose_target.position.z = pose_target.position.z - offset_z
    print "===== tunning object position "
    print ">> tunned pose: ", pose_target
    #print "x: ", pose_target.position.x
    #print "y: ", pose_target.position.y
    #print "z: ", pose_target.position.z
    #print "ox: ", pose_target.orientation.x
    #print "oy: ", pose_target.orientation.y
    #print "oz: ", pose_target.orientation.z
    #print "ow: ", pose_target.orientation.w

    #pose_target.position.x = 0.0247508438196
    #pose_target.position.y = 0.238068788808
    #pose_target.position.z = 0.612619750418
    #pose_target.orientation.x = 0.297805650149
    #pose_target.orientation.y = 0.942263795188
    #pose_target.orientation.z = -0.150006471888
    #pose_target.orientation.w = 0.0308024902946


    #pose_rel.position.y = pose_rel.position.y+0.1
    #group.set_pose_target(pose_rel)

    #pose_random = group.get_random_pose()
    #print pose_random

    #group.set_pose_target(pose_random)

    #group.set_pose_target(pose_target)
    self.group.set_position_target([pose_target.position.x, pose_target.position.y, pose_target.position.z])

    #print group.get_known_constraints()

    ## Now, we call the planner to compute the plan
    ## and visualize it if successful
    ## Note that we are just planning, not asking move_group
    ## to actually move the robot
    plan1 = self.group.plan()
    if len(plan1.joint_trajectory.points) > 0:
      print "******* A plan was found ********"
      #print plan1
      self.group.go(wait=True)

      ##
      ## This is a push action, we use the current joint values
      ## for a relative movement
      ##
      print "============ Trying to push using current joint values: "
      #group_variable_values = self.group.get_current_joint_values()

      ## Now, let's modify one of the joints, plan to the new joint
      ## space goal and visualize the plan
      #group_variable_values[2] = group_variable_values[2]+0.2
      #self.group.set_joint_value_target(group_variable_values)

      push_distance_y = offset_y + 0.06

      pose_target.position.y = pose_target.position.y + push_distance_y
      print ">> push position: ", pose_target.position

      self.group.set_position_target([pose_target.position.x, pose_target.position.y, pose_target.position.z])

      plan2 = self.group.plan()
      if len(plan2.joint_trajectory.points) > 0:
        print "******* A plan for pushing was found ********"
        self.group.go(wait=True)
      else:
        print "-------- no push is possible -------"

    else:
      print "------- No plan was found -------"
    #self.now_executing = False

    ##print "============ Waiting while RVIZ displays plan1..."
    #rospy.sleep(5)


    ## You can ask RVIZ to visualize a plan (aka trajectory) for you.  But the
    ## group.plan() method does this automatically so this is not that useful
    ## here (it just displays the same trajectory again).
    #print "============ Visualizing plan1"
    #display_trajectory = moveit_msgs.msg.DisplayTrajectory()

    #display_trajectory.trajectory_start = robot.get_current_state()
    #display_trajectory.trajectory.append(plan1)
    #display_trajectory_publisher.publish(display_trajectory);

    #print "============ Waiting while plan1 is visualized (again)..."
    #rospy.sleep(5)



    if False:
        ## Moving to a pose goal
        ## ^^^^^^^^^^^^^^^^^^^^^
        ##
        ## Moving to a pose goal is similar to the step above
        ## except we now use the go() function. Note that
        ## the pose goal we had set earlier is still active
        ## and so the robot will try to move to that goal. We will
        ## not use that function in this tutorial since it is
        ## a blocking function and requires a controller to be active
        ## and report success on execution of a trajectory.

        # Uncomment below line when working with a real robot
        # group.go(wait=True)

        ## Planning to a joint-space goal
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        print "============ Planning to a joint-space goal"
        ##
        ## Let's set a joint space goal and move towards it.
        ## First, we will clear the pose target we had just set.

        self.group.clear_pose_targets()

        ## Then, we will get the current set of joint values for the group
        group_variable_values = self.group.get_current_joint_values()
        print "============ Joint values: ", group_variable_values

        ## Now, let's modify one of the joints, plan to the new joint
        ## space goal and visualize the plan
        group_variable_values[0] = -2.0
        self.group.set_joint_value_target(group_variable_values)

        plan2 = self.group.plan()

        # uncomment when working with real robot
        self.group.go(wait=True)

        #print "============ Waiting while RVIZ displays plan2..."
        #rospy.sleep(5)


    self.go_to_initial_position()
    print "============ END OF MOVEMENT"
    print "Continue ?"
    tmp = raw_input()
    self.now_executing = False
        ## When finished shut down moveit_commander.
        #moveit_commander.roscpp_shutdown()



if __name__=='__main__':
  rospy.init_node('katana_move_to_marker', anonymous=True)
  object_tf = rospy.get_param('~object_tf', '/object_1')
  node = MoverMarker(object_tf)

  try:
    #move_group_python_interface_katana()
    #node.move_group_python_interface_katana()
    rospy.spin()
  except rospy.ROSInterruptException:
    pass
  ## When finished shut down moveit_commander.
  moveit_commander.roscpp_shutdown()

# How to use katana

## Bringing katana up

`Copy here the little manual made in the virtual machine`

### When calibration is needed

Imagine this setup:
 * Katana is on the table
 * An external kinect camera is beside the katana looking over the workspace

In this case we need to find the extrinsic calibration between the camara link and the katana base link. We assume that the world frame is the base link (using a dummy joint).

1. Bring up the kinect camera. This is done using either the openni2 driver or the freenect driver from ros. I prefer the second. 

`roslaunch freenect_launch freenect.launch`

2. We use alver markers to find the specific orientation of a marker on the camera frame. You can dowload this package from ros repository or from the source. I prefer the source because it gives more flexibility when customizing certain files. I modified version of the pr2_bundle.launch file was created to fit our setup: katana_bundle.launch. 

Print the png with the markers bundle put it on the table in front of the katana and visible from the kinect (use rviz to be sure it is visible). The position of the marker 8 (the first of the buble from left to right/top to bottom) needs to be known. Rotation is unchanged. 

We create a dummy transformation from a fake /target link to the /katana_base_link:

`rosrun tf static_transform_publisher -0.273 0.0 0.0 0.0 0.0 0.0 /target /katana_base_link 100`

We create a transformation from the /ar_marker_8 link that the alvar marker node is going to detect on the table to the dummy link /target. This transformation will connect the two trees from camera and katana.

`rosrun tf static_transform_publisher 0.0 0.0 0.0 0.0 0.0 0.0 /ar_marker_8 /target 100`

It is a null transformation. We achieved this by positioning the marker8 in the far front of the katana (with the marker9 at the right and the marker10 close to the katana base) and aligned (by the middle of the marker) to the katana_base_link:

            [katana]

[no marker] [marker10]
[marker9]   [marker8]

3. Now we launched the alvar_marker tracker and wait:

`roslaunch ar_track_alvar katana_bundle.launch`

If we open rviz and visualize the robot_model and the cloud of points using the katana_base_link as fixedframe we will see how after some seconds the cloud of points will be in the correct position.

4. Once the cloud of points is aligned we recover the transformation between the katana_base_link and the camera_link as the extrinsic calibration

`rosrun tf tf_echo /katana_base_link /camera_link`

The results will give us the transformation we need, e.g.:

`- Translation: [0.253, -0.446, 0.499]`
`- Rotation: in Quaternion [-0.306, 0.305, 0.656, 0.619]`
`            in RPY (radian) [0.034, 0.894, 1.644]`
`            in RPY (degree) [1.923, 51.241, 94.211]`

5. Stop all the calibration processes: the custom static transformations and the alvar marker launch. This will stop the visualization in rviz.

6. With the recovered calibration we can either modify our bringup launch file or use a static transformation to finish our setup:

`rosrun tf static_transform_publisher 0.253 -0.446 0.499 -0.306 0.305 0.656 0.619 /katana_base_link /camera_link 100`

This example uses the quaternion values for the custom static transformation.

*Remeber that when using the RPY values of the rotation, ros performs the rotation in the order YPR* 

This command will restart the visualization in rviz, which indicates the process of calibration is completed.






3.

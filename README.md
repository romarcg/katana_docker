# Implementation of affordance learning for Katana 400_6m180 arm

> **Notes**:
> Currently, only works with the real robotic arm.
> Modifications are needed to make it work using the arm simulator.
> Simulator files are included in the roskatanafullindigo docker file.


> **Disclaimer**:
> This code is provided only as a tool to reproduce the experimentals setup for the corresponding publication.

# Publication

This code is part of the experimental evaluation (using the Katana arm) for the publication:

**Affordance equivalence relationship: discovery and learning**

*Authors*
